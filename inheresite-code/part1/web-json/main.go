package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"
)

type MyYear struct {
	CurrentYear int    `json:"currentyear"`
	Good        bool   `json:"good,omitempty"`
	Comment     string `json:"comment,omitempty"`
}

func getCurrentYear(w http.ResponseWriter, r *http.Request) {
	currentTime := time.Now()
	year := currentTime.Year()
	var goodYear bool
	var comment string
	if year == 2020 || year == 2021 {
		goodYear = false
		comment = "big oof"
	}
	myCurrentYear := MyYear{
		CurrentYear: year,
		Good:        goodYear,
		Comment:     comment,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(myCurrentYear)
	return
}

func main() {
	log.Println("ordering facilities online")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	http.HandleFunc("/year", getCurrentYear)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
