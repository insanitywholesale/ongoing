package main

import (
	"log"
	"net/http"
)

func main() {
	router := MakeRouter()
	log.Fatal(http.ListenAndServe(":8000", router))
}
