package main

import (
	"database/sql"

	_ "github.com/jackc/pgx/v4/stdlib"
)

type postgresDB struct {
	client *sql.DB
	pgURL  string
}

func newPostgresClient(url string) (*sql.DB, error) {
	client, err := sql.Open("pgx", url)
	if err != nil {
		return nil, err
	}
	err = client.Ping()
	if err != nil {
		return nil, err
	}
	_, err = client.Exec(createDeliveriesTableQuery)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewPostgresDB(url string) (*postgresDB, error) {
	pgclient, err := newPostgresClient(url)
	if err != nil {
		return nil, err
	}
	db := &postgresDB{
		pgURL:  url,
		client: pgclient,
	}
	return db, nil
}

func (pdb *postgresDB) ReturnAll() ([]*Delivery, error) {
	deliveryslice := []*Delivery{}
	rows, err := pdb.client.Query(retrieveAllDeliveriesQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		delivery := &Delivery{}
		err = rows.Scan(
			&delivery.OrderNumber,
			&delivery.City,
			&delivery.Zipcode,
			&delivery.Address,
			&delivery.Phone1,
			&delivery.Phone2,
			&delivery.Cancelled,
			&delivery.Delivered,
			&delivery.DeliveryAttempts,
			&delivery.Driver.FirstName,
			&delivery.Driver.LastName,
		)
		if err != nil {
			return nil, err
		}
		deliveryslice = append(deliveryslice, delivery)
	}
	return deliveryslice, nil
}

func (pdb *postgresDB) ReturnOne(orderNumber int) (*Delivery, error) {
	return &Delivery{}, nil
}

func (pdb *postgresDB) Store(d *Delivery) (*Delivery, error) {
	return &Delivery{}, nil
}

func (pdb *postgresDB) Change(orderNumber int, del *Delivery) (*Delivery, error) {
	return &Delivery{}, nil
}

func (pdb *postgresDB) Remove(orderNumber int) error {
	return nil
}
