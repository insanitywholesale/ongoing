package main

var createDeliveriesTableQuery = `CREATE TABLE IF NOT EXISTS Deliveries (
	OrderNumber SERIAL PRIMARY KEY,
	City VARCHAR,
	Zipcode VARCHAR,
	Address VARCHAR,
	Phone1 VARCHAR,
	Phone2 VARCHAR,
	Cancelled BOOL,
	Delivered BOOL,
	DeliveryAttempts INTEGER,
	DriverFirstName VARCHAR,
	DriverLastName VARCHAR
);`

var retrieveAllDeliveriesQuery = `SELECT 
	OrderNumber,
	City,
	Zipcode,
	Address,
	Phone1,
	Phone2,
	Cancelled,
	Delivered,
	DeliveryAttempts,
	DriverFirstName,
	DriverLastName FROM Deliveries;`

var retrieveOneDeliveryQuery = `SELECT
	OrderNumber,
	City,
	Zipcode,
	Address,
	Phone1,
	Phone2,
	Cancelled,
	Delivered,
	DeliveryAttempts,
	DriverFirstName,
	DriverLastName
FROM Deliveries WHERE OrderNumber=$1;`

var storeDeliveryQuery = `INSERT INTO Deliveries (
	City,
	Zipcode,
	Address,
	Phone1,
	Phone2,
	Cancelled,
	Delivered,
	DeliveryAttempts,
	DriverFirstName,
	DriverLastName
) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING OrderNumber;`
