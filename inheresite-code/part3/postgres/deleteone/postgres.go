package main

import (
	"database/sql"

	_ "github.com/jackc/pgx/v4/stdlib"
)

type postgresDB struct {
	client *sql.DB
	pgURL  string
}

func newPostgresClient(url string) (*sql.DB, error) {
	client, err := sql.Open("pgx", url)
	if err != nil {
		return nil, err
	}
	err = client.Ping()
	if err != nil {
		return nil, err
	}
	_, err = client.Exec(createDeliveriesTableQuery)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewPostgresDB(url string) (*postgresDB, error) {
	pgclient, err := newPostgresClient(url)
	if err != nil {
		return nil, err
	}
	db := &postgresDB{
		pgURL:  url,
		client: pgclient,
	}
	return db, nil
}

func (pdb *postgresDB) ReturnAll() ([]*Delivery, error) {
	deliveryslice := []*Delivery{}
	rows, err := pdb.client.Query(retrieveAllDeliveriesQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		delivery := &Delivery{}
		err = rows.Scan(
			&delivery.OrderNumber,
			&delivery.City,
			&delivery.Zipcode,
			&delivery.Address,
			&delivery.Phone1,
			&delivery.Phone2,
			&delivery.Cancelled,
			&delivery.Delivered,
			&delivery.DeliveryAttempts,
			&delivery.Driver.FirstName,
			&delivery.Driver.LastName,
		)
		if err != nil {
			return nil, err
		}
		deliveryslice = append(deliveryslice, delivery)
	}
	return deliveryslice, nil
}

func (pdb *postgresDB) ReturnOne(orderNumber int) (*Delivery, error) {
	delivery := &Delivery{}
	err := pdb.client.QueryRow(retrieveOneDeliveryQuery, orderNumber).Scan(
		&delivery.OrderNumber,
		&delivery.City,
		&delivery.Zipcode,
		&delivery.Address,
		&delivery.Phone1,
		&delivery.Phone2,
		&delivery.Cancelled,
		&delivery.Delivered,
		&delivery.DeliveryAttempts,
		&delivery.Driver.FirstName,
		&delivery.Driver.LastName,
	)
	if err != nil {
		return nil, err
	}
	return delivery, nil
}

func (pdb *postgresDB) Store(d *Delivery) (*Delivery, error) {
	err := pdb.client.QueryRow(storeDeliveryQuery,
		d.City,
		d.Zipcode,
		d.Address,
		d.Phone1,
		d.Phone2,
		d.Cancelled,
		d.Delivered,
		d.DeliveryAttempts,
		d.Driver.FirstName,
		d.Driver.LastName,
	).Scan(&d.OrderNumber)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func (pdb *postgresDB) Change(orderNumber int, del *Delivery) (*Delivery, error) {
	_, err := pdb.client.Exec(changeDeliveryQuery,
		orderNumber,
		del.City,
		del.Zipcode,
		del.Address,
		del.Phone1,
		del.Phone2,
		del.Cancelled,
		del.Delivered,
		del.DeliveryAttempts,
		del.Driver.FirstName,
		del.Driver.LastName,
	)
	if err != nil {
		return nil, err
	}
	return pdb.ReturnOne(orderNumber)
}

func (pdb *postgresDB) Remove(orderNumber int) error {
	_, err := pdb.client.Exec(removeDeliveryQuery, orderNumber)
	if err != nil {
		return err
	}
	return nil
}
