package main

import (
	"log"
	"net/http"
	"os"
)

var deliverydb DeliveryDB

func main() {
	// Create a database
	pgURL := os.Getenv("PG_URL")
	if pgURL != "" {
		db, err := NewPostgresDB(pgURL)
		if err != nil {
			log.Fatalf("error with postgres connection %v", err)
		}
		deliverydb = db
		log.Println("connected to postgres")
	} else {
		deliverydb = NewListDatabase()
		log.Println("connected to listdb")
	}
	// Start http server
	router := MakeRouter()
	log.Fatal(http.ListenAndServe(":8000", router))
}
