package main

import (
	"log"
	"net/http"
)

var deliverydb DeliveryDB

func main() {
	// Create a database
	deliverydb = NewListDatabase()
	// Start http server
	router := MakeRouter()
	log.Fatal(http.ListenAndServe(":8000", router))
}
