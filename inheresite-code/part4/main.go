package main

import (
	"log"
	"net/http"
	"os"
)

var deliverydb DeliveryDB

func CreateDB() DeliveryDB {
	pgURL := os.Getenv("PG_URL")
	if pgURL != "" {
		db, err := NewPostgresDB(pgURL)
		if err != nil {
			log.Fatalf("error with postgres connection %v", err)
		}
		log.Println("connected to postgres")
		return db
	} else {
		log.Println("connected to listdb")
		return NewListDatabase()
	}
}

func main() {
	// Create a database
	deliverydb = CreateDB()
	// Crete mux
	router := MakeRouter()
	// Start http server
	log.Fatal(http.ListenAndServe(":8000", router))
}
