package main

type Thing struct {
	Data struct {
		Children []struct {
			Data struct {
				DisplayName string `json:"display_name"`
				Title       string `json:"title"`
				URL         string `json:"url"`
			} `json:"data"`
		} `json:"children"`
	} `json:"data"`
}
