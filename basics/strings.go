package main

import (
	"fmt"
)

func main() {
	s := "98765432"
	for i, v := range s {
		fmt.Println("i:", i)
		fmt.Println("v:", int(v))
	}
}
