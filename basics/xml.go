package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

type SitemapIndex struct {
	Locations []Location `xml:"url"`
}

type Location struct {
	Loc string `xml:"loc"`
}

func (l Location) String() string {
	return fmt.Sprintf(l.Loc)
}

func indexHandler() {
	fmt.Fprintf(w, "<html><head><title>Go Webapp Index</title></head><body><h1>Go is neat!</h1></body></html>")
}

func articleAggHandler() {
	resp, _ := http.Get("https://passthroughpo.st/post-sitemap.xml") // #represent
	bytes, _ := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	var s SitemapIndex
	xml.Unmarshal(bytes, &s)

	// fmt.Println(s.Locations)

	//print the links to the cli
	//for and range syntax:
	//for index value, value of thing being iterated over := range thing being iterated over {
	//for _, Location := range s.Locations {
	//	fmt.Printf("%s\n", Location)
	//}
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/agg/", articleAggHandler)
	http.ListenAndServe(":9090", nil)
}
