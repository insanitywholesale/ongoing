package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

var (
	highest int
	lowest  int
	win     = false
	random  int
)

func verify(in int) int {
	if in == random {
		return 0
	} else if in > random {
		return 1
	} else {
		return -1
	}
}

func findNumber() int {
	var guess int
	guess = 500000
	for i := 0; i <= 51; i++ {
		if i == 51 {
			win = true
			// fmt.Println("CON poo ter")
		} else {
			guess = ((lowest + highest) / 2)
			state := verify(guess)
			if state == 1 {
				highest = guess
			} else if state == -1 {
				lowest = guess
			} else if state == 0 {
				// fmt.Println("Gomputer won")
				return guess
			}
		}
	}
	return guess
}

func main() {
	highest = 1000000
	lowest = 0
	rand.Seed(time.Now().UnixNano())
	random = rand.Intn(highest-lowest) + lowest
	_ = findNumber()
	if win {
		fmt.Println("fucking finally")
		os.Exit(0)
	} else {
		os.Exit(1)
	}
}
