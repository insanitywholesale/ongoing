package main

import (
	"fmt"
)

func foo(c chan int, bar int) {
	c <- bar * 10
}

func main() {
	fooChannel := make(chan int)
	go foo(fooChannel, 5)
	go foo(fooChannel, 2)

	baz1, baz2 := <-fooChannel, <-fooChannel

	fmt.Println(baz1, baz2)
}
