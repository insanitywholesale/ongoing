package main

func main() {
	for i := 0; i <= 3; i++ { // normal loop
		println(i)
	}
	for { // infinite loop
		break
	}
	j := 0
	for j < 5 {
		println(j)
		j += 2
	}
}
