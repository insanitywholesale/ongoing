package main

import (
	"fmt"
)

func main() {
	ch := make(chan string, 2)
	ch <- "one"
	ch <- "two"
	fmt.Println(<-ch)
	ch <- "three"
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}
