package main

import (
	"encoding/xml"
	"fmt"
	"io/fs"
	"log"
	"os"
)

type ContactEducation struct {
	Degree       string `xml:"Degree"` // Bachelors or Masters
	GradYear     string `xml:"GradYear"`
	HasGraduated bool   `xml:"HasGraduated"`
	Major        string `xml:"Major"`
	School       string `xml:"School"`
}

func (ce ContactEducation) String() string {
	sep := " | "
	return fmt.Sprint(ce.GradYear + sep + ce.Degree + sep + ce.Major + sep + ce.School)
}

type Education struct {
	ContactEducationList []ContactEducation `xml:"ContactEducation"` // There may be multiple ContactEducation
}

func main() {
	fileNames, err := fs.Glob(os.DirFS("."), "*.xml")
	if err != nil {
		log.Fatal("oopsie globbing directory or files", err)
	}
	for i, fileName := range fileNames {
		content, err := fs.ReadFile(os.DirFS("."), fileName)
		if err != nil {
			log.Fatal("oopsie reading file", err)
		}
		var e Education
		xml.Unmarshal(content, &e)
		fmt.Println("Record", i+1, ":-")
		for _, ce := range e.ContactEducationList {
			fmt.Println(ce)
		}
	}
}
