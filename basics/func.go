package main

import "fmt"

func add(x int, y int) {
	fmt.Println(x + y)
}

func mult(x, y int) int {
	return x * y
}

func div(x, y int) (float64, error) {
	if y == 0 {
		return -1, fmt.Errorf("division by zero not allowed")
	} else {
		return x / y, nil
	}
}
