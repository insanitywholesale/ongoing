package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func foo(c chan int, bar int) {
	defer wg.Done()
	c <- bar * 10
}

func main() {
	fooChannel := make(chan int, 10)

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go foo(fooChannel, i)
	}
	wg.Wait()
	close(fooChannel)

	for item := range fooChannel {
		fmt.Println(item)
	}
}
