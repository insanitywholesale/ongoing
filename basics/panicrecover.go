package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func cleanup() {
	defer wg.Done()
	if r := recover(); r != nil {
		fmt.Println("recovered in cleanup:", r)
	}
}

func foo() {
	defer fmt.Println("done!")
	defer fmt.Println("are we done?")
	fmt.Println("doing some stuff")

	for i := 0; i < 5; i++ {
		defer fmt.Println(i)
	}
}

func say(s string) {
	defer cleanup()
	for i := 0; i < 3; i++ {
		fmt.Println(s)
		time.Sleep(time.Millisecond * 100)
		if i == 2 {
			panic("oh dear, a 2")
		}
	}
}

func main() {
	wg.Add(1)
	go say("hey")
	wg.Add(1)
	go say("there")

	wg.Add(1)
	go say("Hi")
	wg.Wait()
	foo()
}
