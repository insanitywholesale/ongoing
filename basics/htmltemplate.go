package main

import (
	"fmt"
	"html/template"
	"net/http"
	//"encoding/xml"
	//"io/ioutil"
)

type ArticleAggPage struct {
	Modified string
	Article  string
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<html><h1>whoa go cool</h1></html>")
}

func articleAggHandler(w http.ResponseWriter, r *http.Request) {
	p := ArticleAggPage{Modified: "Date", Article: "some article"}
	t, _ := template.ParseFiles("basictemplating.html")
	t.Execute(w, p)
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/agg/", articleAggHandler)
	http.ListenAndServe(":8080", nil)
}
