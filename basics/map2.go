package main

import "fmt"

func main() {
	var coffeesPrices map[string]float32
	coffeesPrices = make(map[string]float32)
	coffeesPrices["freddo espresso"] = 1.20
	coffeesPrices["cappuccino"] = 1.35

	for name, price := range coffeesPrices {
		fmt.Println("coffee:", name, "price:", price)
	}
}
