package main

import (
	"encoding/gob"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	fmt.Println("----program start----")
	grades := make(map[string]float32)

	grades["Lovelace"] = 100
	grades["Timmy"] = 42
	grades["Jess"] = 89
	grades["Laf"] = 57

	fmt.Println("\n----All Grades----")
	for key, val := range grades {
		fmt.Println("name", key, "grade", val)
	}

	// Eliminate the weak
	for name, grade := range grades {
		if grade < 85 {
			delete(grades, name)
			fmt.Println("\ndeleted", name)
		}
	}

	fmt.Println("\n----Best Grades----")
	for key, val := range grades {
		fmt.Println("name", key, "grade", val)
	}

	// Write the grades map to file
	f, err := os.Create("/tmp/grades-file")
	check(err)
	encoder := gob.NewEncoder(f).Encode(grades)
	check(encoder)
}
