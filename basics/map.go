package main

import "fmt"

func main() {
	grades := make(map[string]float32)

	grades["Timmy"] = 42
	grades["Jess"] = 69
	grades["Laf"] = 57

	// fmt.Println(grades)
	for key, val := range grades {
		fmt.Println("name", key, "grade", val)
	}

	TimmyGrade := grades["Timmy"]
	fmt.Println("Timmy's grade is too low", TimmyGrade)

	delete(grades, "Timmy")
	fmt.Println(grades)

	for key, val := range grades {
		fmt.Println("name", key, "grade", val)
	}
}
