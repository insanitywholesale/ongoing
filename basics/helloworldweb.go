package main

import (
	"log"
	"net/http"
)

func helloworldHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`<!DOCTYPE html>
	<head>
		<title>Hello World</title>
	</head>
	<body>
		<center><h1>Hello World</h1></center>
	</body>
</html>
`))
}

func main() {
	http.HandleFunc("/", helloworldHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
