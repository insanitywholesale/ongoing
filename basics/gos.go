package main

import (
	"fmt"
)

const (
	usixteenbitmax float64 = 65535
	kmh_multiple   float64 = 1.60934
)

type rc_car struct {
	accel         uint16
	deccel        uint16
	steer         int16
	top_speed_kmh float64
}

func (c rc_car) kmh() float64 {
	return float64(c.accel) * (c.top_speed_kmh / usixteenbitmax)
}

func (c rc_car) mph() float64 {
	return float64(c.accel) * (c.top_speed_kmh / usixteenbitmax / kmh_multiple)
}

func (c *rc_car) new_top_speed(new_speed float64) {
	c.top_speed_kmh = new_speed
}

func newer_top_speed(c rc_car, speed float64) rc_car {
	c.top_speed_kmh = speed
	return c
}

func main() {
	rc_car_01 := rc_car{
		accel:         22841,
		deccel:        0,
		steer:         8901,
		top_speed_kmh: 122.7,
	}
	rc_car_02 := rc_car{
		64841,
		0,
		1201,
		129.7,
	}

	fmt.Println("----RC car 1 before----")
	fmt.Println("accel:", rc_car_01.accel)
	fmt.Println(rc_car_01.kmh())
	fmt.Println(rc_car_01.mph())
	fmt.Println("-----RC car 1 after-----")
	rc_car_01.new_top_speed(420.69)
	fmt.Println(rc_car_01.kmh())
	fmt.Println(rc_car_01.mph())

	fmt.Println("----RC car 2 before----")
	fmt.Println("accel:", rc_car_02.accel)
	fmt.Println(rc_car_02.kmh())
	fmt.Println(rc_car_02.mph())
	fmt.Println("-----RC car 2 after-----")
	rc_car_02 = newer_top_speed(rc_car_02, 69.69)
	fmt.Println(rc_car_02.kmh())
	fmt.Println(rc_car_02.mph())
}
