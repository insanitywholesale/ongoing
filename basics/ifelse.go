package main

func main() {
	s := "w"
	b := true
	if s != "" && b {
		println("if")
	} else if s == "" && !b {
		println("else if")
	} else {
		println("else")
	}
}
