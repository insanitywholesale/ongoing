package main

import (
	"fmt"

	"github.com/kavehmz/prime"
)

// some potentially interesting number: rawPrimes[i]*rawPrimes[j]

func main() {
	// rawPrimes := prime.Primes(133)
	rawPrimes := prime.Primes(55)
	fmt.Printf("rawPrimes: %v\n\n", rawPrimes)

	multipliedPrimes := make([]uint64, len(rawPrimes))
	copy(multipliedPrimes, rawPrimes)

	i := 0
	j := uint64(0)
	for i < len(rawPrimes) && j < uint64(len(rawPrimes)) {
		fmt.Println("iteration", i)
		fmt.Println("j", j)

		multipliedPrimes[j] *= rawPrimes[i]

		i++

		// j += rawPrimes[i] // set index to sum of primes
		j = rawPrimes[i] // set index to prime

		fmt.Printf("multipliedPrimes: \n\t%v\n", multipliedPrimes)
	}
}
