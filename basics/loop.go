package main

import "fmt"

func main() {
	x := 5
	for {
		fmt.Println("hello", x)
		x += 3
		if x > 25 {
			break
		}
	}

	fmt.Println("hello, nil", nil)

	for x := 5; x < 25; x += 3 {
		fmt.Println("hi", x)
	}

	fmt.Println("hi, nil", nil)
}
