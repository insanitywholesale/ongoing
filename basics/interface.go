package main

import "fmt"

type alnum string

func main() {
	var i interface{}
	fmt.Println(i)
	var s alnum = "a string"
	i = s
	fmt.Println(i.(alnum))
}
