package main

import (
	"fmt"
	"net/http"
)

func index_handler(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "<h3>Cards</h3><br/><h5>List</h5>")
	fmt.Fprintf(w, "Cards\nList")
}

func about_handler(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "Me<sub>nothing interesting</sub>")
	fmt.Fprintf(w, "Me, nothing interesting")
}

func main() {
	http.HandleFunc("/", index_handler)
	http.HandleFunc("/about/", about_handler)
	http.ListenAndServe(":8012", nil)
}
