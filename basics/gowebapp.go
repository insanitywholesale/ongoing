package main

import (
	"encoding/xml"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"sync"
)

var wg sync.WaitGroup

type ArticleAggPage struct {
	Modified string
	Article  map[string]ArticleMap
}

type ArticleMap struct {
	Location string
}

type SitemapIndex struct {
	Locations []string `xml:"sitemap>loc"`
}

type Articles struct {
	Modified  []string `xml:"url>lastmod"`
	Locations []string `xml:"url>loc"`
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<html><head><title>Go Web App index</title></head><body style='background-color: #c8a2c8'><center><h1>Whoa, Go is neat!</h1></center></body></html>")
}

func articleRoutine(c chan Articles, Location string) {
	defer wg.Done()

	var n Articles

	resp, _ := http.Get(Location)
	bytes, _ := ioutil.ReadAll(resp.Body)
	xml.Unmarshal(bytes, &n)
	resp.Body.Close()
	c <- n
}

func articleAggHandler(w http.ResponseWriter, r *http.Request) {
	var s SitemapIndex

	resp, _ := http.Get("https://passthroughpo.st/sitemap_index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	xml.Unmarshal(bytes, &s)
	articleMap := make(map[string]ArticleMap)
	resp.Body.Close()
	queue := make(chan Articles, 500)

	// for index value, value of thing being iterated over := range thing being iterated over {
	for _, Location := range s.Locations {
		wg.Add(1)
		go articleRoutine(queue, Location)

	}
	wg.Wait()
	close(queue)

	for item := range queue {
		for idx := range item.Modified {
			articleMap[item.Locations[idx]] = ArticleMap{item.Modified[idx]}
		}
	}

	p := ArticleAggPage{Modified: "Date", Article: articleMap} // should probably change modified date to be a dynamic vale
	t, _ := template.ParseFiles("articleaggtemplate.html")
	t.Execute(w, p)
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/agg/", articleAggHandler)
	http.ListenAndServe(":8080", nil)
}
