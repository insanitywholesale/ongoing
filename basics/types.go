package main

var b bool

var (
	i   int // 32 or 64 depending on platform
	i8  int8
	i16 int16
	i32 int32
	i64 int64
)

var (
	ui   uint // 32 or 64 depending on platform
	ui8  uint8
	ui16 uint16
	ui32 uint32
	ui64 uint64
)

var (
	f32 float32
	f64 float64
)

var s string

var (
	r    rune // alias for int32
	bui8 byte // alias for uint8
)
