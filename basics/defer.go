package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func foo() {
	defer fmt.Println("done!")
	defer fmt.Println("are we done?")
	fmt.Println("doing some stuff")

	for i := 0; i < 5; i++ {
		defer fmt.Println(i)
	}
}

func say(s string) {
	defer wg.Done()
	for i := 0; i < 3; i++ {
		fmt.Println(s)
		time.Sleep(time.Millisecond * 100)
	}
}

func main() {
	wg.Add(1)
	go say("hey")
	wg.Add(1)
	go say("there")

	wg.Add(1)
	go say("Hi")
	wg.Wait()
	foo()
}
