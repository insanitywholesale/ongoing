package main

import (
	"log/slog"
	"net/http"
	"os"
	"time"

	probing "github.com/prometheus-community/pro-bing"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var internetGauge = prometheus.NewGauge(prometheus.GaugeOpts{
	Namespace: "godude",
	Subsystem: "network",
	Name:      "internet_down",
	Help:      "Number of consecutive ping failures or zero if successful ping",
})

func main() {
	logger := slog.New(slog.NewJSONHandler(os.Stdout, nil))

	// To avoid exposing all default metrics, first create a new custom registry
	customRegistry := prometheus.NewRegistry()

	// And then register only the internet_down metric
	customRegistry.MustRegister(internetGauge)

	// Create a pinger that pings google
	pinger, err := probing.NewPinger("www.google.com")
	if err != nil {
		logger.Info(err.Error())
	}

	// Start continuously pinging google
	go func() {
		for {
			err = pinger.Run() // Blocks until finished.
			if err != nil {
				internetGauge.Inc() // Increment by 1 to record consecutive failed pings
				logger.Info("Internet down: " + err.Error())
			} else {
				internetGauge.Set(0.0) // Set to 0 if up
				logger.Info("Internet up")
			}
			time.Sleep(2 * time.Second) // Take a nap to avoid spam
		}
	}()

	// Expose only internet_down metric on the HTTP server
	http.Handle("/metrics", promhttp.HandlerFor(customRegistry, promhttp.HandlerOpts{}))

	// At this point we're ready to start serving so log that
	logger.Info("Initializing D.U.D.E: DSL Unreability Detection Environment")
	logger.Info("Metrics will be available shortly on port 4345 at /metrics")

	// Start serving
	err = http.ListenAndServe(":4345", nil)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
}
