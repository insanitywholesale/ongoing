package main

import (
	"context"
	"log"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
)

var ctx = context.Background()

func main() {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		log.Fatal(err)
	}

	hostConfig := &container.HostConfig{
		PortBindings: nat.PortMap{
			nat.Port("5432"): []nat.PortBinding{
				{
					HostIP:   "0.0.0.0",
					HostPort: "5432",
				},
			},
		},
	}

	containerConfig := &container.Config{
		Image: "postgres:latest",
		ExposedPorts: nat.PortSet{
			nat.Port("5432"): {},
		},
		Env: []string{"POSTGRES_USER=tester", "POSTGRES_PASSWORD=Apasswd"},
	}

	networkingConfig := &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			"network": {
				NetworkID: "bridge",
			},
		},
	}

	c, err := cli.ContainerCreate(context.TODO(), containerConfig, hostConfig, networkingConfig, nil, "testpostgres")
	if err != nil {
		log.Fatal("create container error:", err)
	}
	log.Println(c)

	err = cli.ContainerStart(context.TODO(), c.ID, container.StartOptions{})
	if err != nil {
		log.Fatal("start container error:", err)
	} else {
		log.Println("container", c.ID, "is created and started")
	}
}
