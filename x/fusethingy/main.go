package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
)

type FileSystem struct {
	urls []string
}

type File struct {
	name string
	url  string
}

func main() {
	if len(os.Args) < 3 {
		log.Fatalf("Usage: %s MOUNTPOINT URL1 [URL2 ... URLn]\n", os.Args[0])
	}
	mountpoint := os.Args[1]
	urls := os.Args[2:]

	c, err := fuse.Mount(
		mountpoint,
		fuse.FSName("urlfs"),
		fuse.Subtype("urlfs"),
		// fuse.LocalVolume(),
		// fuse.VolumeName("URL Filesystem"),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	filesystem := &FileSystem{urls: urls}

	err = fs.Serve(c, filesystem)
	if err != nil {
		log.Panic(err)
	}
}

func (fsys *FileSystem) Root() (fs.Node, error) {
	return &Dir{fsys: fsys}, nil
}

type Dir struct {
	fsys *FileSystem
}

func (d *Dir) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 1
	a.Mode = os.ModeDir | 0o555
	return nil
}

func (d *Dir) Lookup(ctx context.Context, name string) (fs.Node, error) {
	for _, url := range d.fsys.urls {
		if strings.HasSuffix(url, name) {
			return &File{name: name, url: url}, nil
		}
	}
	return nil, fuse.ENOENT
}

func (d *Dir) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	var dirents []fuse.Dirent
	for _, url := range d.fsys.urls {
		parts := strings.Split(url, "/")
		name := parts[len(parts)-1]
		dirents = append(dirents, fuse.Dirent{Name: name, Type: fuse.DT_File})
	}
	return dirents, nil
}

func (f *File) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = 2
	a.Mode = 0o444

	// Get file size by making a HEAD request
	resp, err := http.Head(f.url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	a.Size = uint64(resp.ContentLength)
	a.Mtime = time.Now()
	a.Ctime = time.Now()
	a.Atime = time.Now()
	return nil
}

func (f *File) ReadAll(ctx context.Context) ([]byte, error) {
	resp, err := http.Get(f.url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}
