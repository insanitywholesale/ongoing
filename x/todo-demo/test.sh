#!/bin/bash

echo "POST"
curl -s -X POST -d '{"description":"number 1 test todo", "done":false}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 2 test todo", "done":false}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 3 test todo", "done":false}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 4 test todo", "done":false}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 5 test todo", "done":false}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 60 test todo", "done":true}' 'http://localhost:8080/todo' | jq -c
curl -s -X POST -d '{"description":"number 7 test todo", "done":true}'  'http://localhost:8080/todo' | jq -c
echo ""
sleep 1;

echo "GET all"
curl -s -X GET 'http://localhost:8080/todos' | jq
echo ""
sleep 1;

echo "GET one"
curl -s -X GET 'http://localhost:8080/todo/2' | jq -c
echo ""
sleep 1;

echo "PUT"
curl -s -X PUT -d '{"description":"number 6 test todo","done":true}' 'http://localhost:8080/todo/6' | jq -c
echo ""
sleep 1;

echo "DELETE"
curl -s -X DELETE 'http://localhost:8080/todo/7' | jq -c
echo ""
sleep 1;

echo "GET all again"
curl -s -X GET 'http://localhost:8080/todos' | jq
echo ""
sleep 1;
