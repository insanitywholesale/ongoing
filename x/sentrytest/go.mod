module ongoing/x/sentrytest

go 1.23.2

require github.com/getsentry/sentry-go v0.29.1

require (
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.19.0 // indirect
)
