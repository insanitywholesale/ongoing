package main

import (
	"bufio"
	"fmt"
	"go/parser"
	"go/token"
	"os"
	"strconv"
	"strings"
)

func main() {
	fileName := os.Getenv("CODE_TUTORIAL_FILE")
	if len(os.Args) > 1 {
		fileName = os.Args[1]
	}
	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}

	var codeblocks [][]string

	scanner := bufio.NewScanner(file)
	var codeblock []string
	readingCodeblock := false
	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, "highlight go") {
			readingCodeblock = true
			continue
		} else if readingCodeblock && strings.Contains(line, "/highlight") {
			readingCodeblock = false
			codeblocks = append(codeblocks, codeblock)
			codeblock = []string{}
			continue
		}

		if readingCodeblock {
			codeblock = append(codeblock, line)
		}
	}

	for i, codeblock := range codeblocks {
		for _, line := range codeblock {
			fmt.Println(line)
		}
		fset := token.NewFileSet()
		_, err := parser.ParseFile(fset, "codeblock "+strconv.Itoa(i), strings.Join(codeblock, "\n"), parser.AllErrors)
		if err != nil {
			fmt.Println(fmt.Errorf("Codeblock contains errors: %w", err))
			os.Exit(1)
		}
	}
}
