package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

func main() {
	res, err := http.Get("https://api.invidious.io/instances.json?sort_by=users,health,api")
	if err != nil {
		log.Panic("get err:", err)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Panic("body read err:", err)
	}

	var result [][]interface{}
	err = json.Unmarshal([]byte(body), &result)
	if err != nil {
		log.Panic("unmarshal err:", err)
	}

	datums := make(map[string]map[string]interface{})
	for _, instance := range result {
		var name string = ""
		for i, b := range instance {
			if i%2 != 0 {
				datums[name] = b.(map[string]interface{})
			} else {
				name = b.(string)
			}
		}
	}
}
